1. Clone the repository:

```bash
git clone https://gitlab.com/mihailodaniliuk/royalty_exchange_scrapper
```
2. Navigate to the project directory:
```bash
cd royalty_exchange_scrapper
```
3. Install the required dependencies:
```bash
pip install -r requirements.txt
```
4. Run the main script of the program:
```bash
python main.py
```
