import time

import requests
import json
import datetime
from datetime import date, datetime
from bs4 import BeautifulSoup
import lxml
import pandas as pd


def get_200_response(url):
    while True:
        try:
            response = requests.get(url)
            if response.status_code != 200:
                continue
        except Exception as e:
            print(e)
            continue
        return response


def get_all_links():
    urls = []
    page = 1
    while True:
        print(f'Page:{page}')
        link = 'https://auctions.royaltyexchange.com/inventory/listings/?filter{state.in}=filled&filter{state.in}=pending&filter{state.in}=closed&sort[]=-deal_date&sort[]=-published_date&page=' + str(
            page) + '&page_size=15'
        try:
            response = requests.get(link)
        except Exception as e:
            print(e)
            continue
        if response.status_code == 200:
            page_json = json.loads(response.text)
            urls.extend([part_data.get('url') for part_data in page_json.get('cached_listings')])
            page += 1

        elif response.status_code == 404:
            break
        else:
            print('Error')
    return urls


all_rights_types = {
    'pro': {
        'rights_type': 'Musical Composition',
        'copyrights_included': 'No',
        'rights': 'Public Performance',
        'sources': 'Internet Streaming, AM/FM & Satellite Radio, TV/Film/Commercial Performances, etc.'
    },

    'publisher': {
        'rights_type': 'Musical Composition',
        'copyrights_included': 'No',
        'rights': '',
        'sources': ''
    },

    'label': {
        'rights_type': 'Sound Recording',
        'copyrights_included': 'No',
        'rights': 'Sales, Streaming , Sync',
        'sources': 'Internet Streaming, Satellite Radio, Digital Downloads, CD Sales, TV/Film/Commercial Placements, Samples, etc.'
    },
    'digital_distributor': {
        'rights_type': 'Sound Recording',
        'copyrights_included': 'No',
        'rights': 'Sales, Streaming',
        'sources': 'Internet Streaming, Digital Downloads, etc.',

    },
    'other': {},
    'custom': {}

}


def get_asset_data(url):
    number = url.replace('https://auctions.royaltyexchange.com/orderbook/asset-detail/', '')
    link = f'https://auctions.royaltyexchange.com/orderbook/api/listings/{number}/?include[]=asset.*&include[]=valuation_description.*&include[]=offers.*'
    asset_data = {}
    # title, tags, last_12_month_earning, three_years_average, dollar_age = '', '', '', '', ''
    # tracks_includes, asset_type, years_remaining, expiration_date, transaction_date = '', '', '', '', ''
    # earning_2023, earning_2022, earning_2021, earning_2020, earning_2019 = '', '', '', '', ''
    # rights_type, copyrights_included, rights, sources = '', '', '', ''
    response = get_200_response(link)
    page_json = json.loads(response.text)
    assets_part_data = list(page_json.get('assets').values())[0]
    # print(page_json)
    asset_data['title'] = page_json.get('listing').get('title')
    asset_data['tags'] = ', '.join(assets_part_data.get('tags'))
    asset_data['last_12_month_earning'] = page_json.get('listing').get('valuation').get('ltm')
    asset_data['three_years_average'] = page_json.get('listing').get('valuation').get('three_years_average')
    asset_data['dollar_age'] = page_json.get('listing').get('valuation').get('dollar_age')
    asset_data['tracks_includes'] = page_json.get('listing').get('track_count')
    asset_data['asset_type'] = page_json.get('listing').get('term').replace('_', ' ').capitalize()
    earning_by_period = page_json.get('listing').get('valuation').get('earnings_by_period')
    if earning_by_period:
        if type(earning_by_period[0][1]) == dict:
            asset_data['earning_2019'] = sum(
                [float(part_data[1].get('all')) for part_data in
                 earning_by_period if
                 part_data[0].startswith('2019')])
            asset_data['earning_2020'] = sum(
                [float(part_data[1].get('all')) for part_data in
                 earning_by_period if
                 part_data[0].startswith('2020')])
            asset_data['earning_2021'] = sum(
                [float(part_data[1].get('all')) for part_data in
                 earning_by_period if
                 part_data[0].startswith('2021')])
            asset_data['earning_2022'] = sum(
                [float(part_data[1].get('all')) for part_data in
                 earning_by_period if
                 part_data[0].startswith('2022')])
            asset_data['earning_2023'] = sum(
                [float(part_data[1].get('all')) for part_data in
                 earning_by_period if
                 part_data[0].startswith('2023')])
        else:
            asset_data['earning_2019'] = sum(
                [float(part_data[1]) for part_data in earning_by_period if part_data[0].startswith('2019')])
            asset_data['earning_2020'] = sum(
                [float(part_data[1]) for part_data in earning_by_period if part_data[0].startswith('2020')])
            asset_data['earning_2021'] = sum(
                [float(part_data[1]) for part_data in earning_by_period if part_data[0].startswith('2021')])
            asset_data['earning_2022'] = sum(
                [float(part_data[1]) for part_data in earning_by_period if part_data[0].startswith('2022')])
            asset_data['earning_2023'] = sum(
                [float(part_data[1]) for part_data in earning_by_period if part_data[0].startswith('2023')])

    royalty_type = assets_part_data.get('royalty_payor').get('kind')
    included_rights = assets_part_data.get('included_rights')
    if included_rights:
        full_rights = {'rights_type': 'Musical Composition' if included_rights.get(
            'musical_composition_rights') else 'Sound Recording'}
        if full_rights.get('rights_type') == 'Sound Recording':
            full_rights['copyrights_included'] = 'Yes' if included_rights.get(
                'sound_recording_copyright_included') else 'No'
            full_rights['rights'] = ', '.join([rights_part.replace('_', ' ').capitalize() for rights_part in
                                               included_rights.get('sound_recording_rights')])
            full_rights['sources'] = included_rights.get('sound_recording_sources')
        else:
            full_rights['copyrights_included'] = 'Yes' if included_rights.get(
                'musical_composition_copyright_included') else 'No'
            full_rights['rights'] = ', '.join([rights_part.replace('_', ' ').capitalize() for rights_part in
                                               included_rights.get('musical_composition_rights')])
            full_rights['sources'] = included_rights.get('musical_composition_sources')
    else:
        full_rights = all_rights_types.get(royalty_type)

    expiration_date_block = page_json.get('listing').get('term_expiration_date')
    if expiration_date_block:
        asset_data['expiration_date'] = expiration_date_block[:10]
        today = date.today()
        date_format = '%Y-%m-%dT%H:%M:%S'
        date_obj = datetime.strptime(expiration_date_block[:19], date_format)
        asset_data['years_remaining'] = round((date_obj.date() - today).days / 365, 2)

    asset_data['rights_type'] = full_rights.get('rights_type')
    asset_data['copyrights_included'] = full_rights.get('copyrights_included')
    asset_data['rights'] = full_rights.get('rights')
    asset_data['sources'] = full_rights.get('sources')
    deal = page_json.get('listing').get('deal')
    if deal:
        asset_data['transaction_price'] = deal.get('amount')
        asset_data['transaction_date'] = deal.get('created_at')[:10]
    top_sources = page_json.get('listing').get('valuation').get('top_sources')
    if top_sources:
        sources_earnings = {source.get('name'): source.get('earnings') for source in top_sources}
        print(sources_earnings)
        asset_data.update(sources_earnings)
    return asset_data


def get_auction_data(url):
    auction_data = {}
    response = get_200_response(url)
    soup = BeautifulSoup(response.text, 'lxml')
    table_block = soup.find('table', class_='es-overview-table')
    trs = table_block.find_all('tr')
    asset_type, last_12_month_earning, royalty_type = '', '', ''
    auction_data['title'] = soup.find('h1', class_='title -auction-page -dark').text
    for tr in trs:
        match tr.find('td').text:
            case 'Investment Term:':
                auction_data['asset_type'] = tr.find_all('td')[1].text
            case 'Royalty Type:':
                auction_data['royalty_type'] = tr.find_all('td')[1].text
            case 'Closing Price:':
                auction_data['transaction_price'] = tr.find_all('td')[1].text
            case "Last 12 Months' Royalties:":
                auction_data['last_12_month_earning'] = tr.find_all('td')[1].text
    return auction_data


if __name__ == '__main__':
    df = pd.DataFrame(columns=['title', 'tags', 'last_12_month_earning', 'three_years_average', 'dollar_age',
                               'tracks_includes', 'asset_type', 'years_remaining', 'expiration_date',
                               'transaction_date',
                               'earning_2023', 'earning_2022', 'earning_2021', 'earning_2020', 'earning_2019',
                               'rights_type', 'copyrights_included', 'rights', 'sources', 'transaction_price',
                               'transaction_date', 'url'])
    links = get_all_links()
    for index, link in list(enumerate(links)):
        # print(link)
        if link.startswith('https://auctions.royaltyexchange.com/orderbook/asset-detail'):
            data = get_asset_data(link)
        else:
            data = get_auction_data(link)
        data['url'] = link
        df.loc[index] = data

        df.to_csv('final.csv')
